from time import sleep
import time
import numpy as np
import cv2
#from parking import parking
import threading
#import SerialHandler
#import SaveEncoder
#import serial,time,sys
from random import randint
#import Camera as cam
import queue

INITIAL = 0
PARKING = 1
RUNNING = 2
CURVE = 3
TRANSITION = 4
INTERSECTION = 5
'''
Vars used:
    Command center:
        -state -> defines the system's state
        -finished -> checks if the car has finished its course, used for the loop

Methods used:
    Command center:
        -printState -> prints state
        -turnOn -> checks the system if it's okay, processes the map
'''
global serialHandler
class CommandCenter:
    def __init__(self):
        self.state = INITIAL
        self.turn_on()
    def car_run(self):
        print("Running")
        cameraQueue =  queue.Queue()
        accelQueue =  queue.Queue()
        gpsQueue = queue.Queue()
        serialHandler = SerialHandler.SerialHandler()
        serialHandler.startReadThread()
        while(True):     
            lastParkSignDistance = 9999
            cameraSign = None 
            cameraThread = threading.Thread(target = cam.getAngle, args(cameraQueue,))
            cameraThread.start()
            time.sleep(0.1)
            #super basic, neoptimizat pt semn (inca)
            #pastram functionalitate pana implementam logica de semn
            #TODO
            while not cameraQueue.empty():
                angle = cameraQueue.get()
                self.move(serialHandler,15-abs(angle),angle)
    def turn_on(self):
        print("Initialising command center:")
        
        #self.car_run()  
    def move(self,serialHandler,speed,angle):
        e = SaveEncoder.SaveEncoder("EncoderPID%.2f"%speed+".csv")
        e.open()
        ev1=threading.Event()
        serialHandler.readThread.addWaiter("MCTL",ev1,e.save)
        sent=serialHandler.sendMove(speed,angle)
        if sent:
            ev1.wait()
            ev1.clear()
            serialHandler.readThread.deleteWaiter("MCTL",ev1)
            time.sleep(0.1)
    #def getSignDistance:
    #def stop():
    #def park():


    def printState(self):
        print(self.state)




x = CommandCenter()


