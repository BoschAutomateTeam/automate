import random
import threading
import queue
import time



def functie1(output1):
    while(1):
        output1.put(random.randint(1,10))
        time.sleep(1.1)
def functie2(output2):
    while(1):
        output2.put(random.randint(20,40))
        time.sleep(1.2)


coada =  queue.Queue()

thread1 = threading.Thread(target = functie1, args = (coada,))
thread1.start()
thread2 = threading.Thread(target = functie2, args = (coada,))
thread2.start()

while (1):
    
    time.sleep(1)
    while not coada.empty():
        print(coada.get())
