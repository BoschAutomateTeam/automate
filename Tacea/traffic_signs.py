import cv2
import numpy as np
stop_cascade = cv2.CascadeClassifier('cascadestop.xml')
park_cascade = cv2.CascadeClassifier('cascadeparking.xml')


cap=cv2.VideoCapture(0)

while True:
    ret, img = cap.read()
    gray=cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    parking =park_cascade.detectMultiScale(gray)
    stop = stop_cascade.detectMultiScale(gray)

    for (x,y,w,h) in parking:
        area=w*h
        if(area>4000):
            font=cv2.FONT_HERSHEY_SIMPLEX
            cv2.putText(img, 'parking',(x+w,y+h), font, 0.5, (0,255,255), 2, cv2.LINE_AA)
            cv2.rectangle(img, (x,y), (x+w, y+h), (255,0,0) ,2)

    for (x,y,w,h) in stop:
        font=cv2.FONT_HERSHEY_SIMPLEX
        cv2.putText(img, 'stop',(x+w,y+h), font, 0.5, (0,255,255), 2, cv2.LINE_AA)
        cv2.rectangle(img, (x,y), (x+w, y+h), (0,0,255) ,2)
        
       
    cv2.imshow('img',img)
    k=cv2.waitKey(30) & 0xff
    if k==27:
        break
cap.release()
cv2.destoyAllWindows()      