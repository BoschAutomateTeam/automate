import json;

class Node:
    def __init__(self,Name,BACK,RIGHT,AHEAD,LEFT,Coordinates):
        self.name=Name
        self.coordinates=Coordinates
        self.neibourgh=[0,0,0,0]

        if BACK == 'none':
            self.neibourgh[0]=0
        else:
            self.neibourgh[0] = str(BACK)
            
        if RIGHT == 'none':
            self.neibourgh[1]=0
        else:
            self.neibourgh[1]=str(RIGHT)
            
        if AHEAD == 'none':
            self.neibourgh[2]=0
        else:
            self.neibourgh[2]=str(AHEAD)
            
        if LEFT == 'none':
            self.neibourgh[3]=0
        else:
            self.neibourgh[3]=str(LEFT)
            
    def getName(self):
        return self.name
	
    def getNeibourgh(self):
        return self.neibourgh
	
    def getCoord(self):
        return self.coordinates
	
	def getNeibourghByPosition(self, pos):
		return self.neibourgh[pos]
            
		
class Map:
    def __init__(self, fileName):
        self.jsonMap=[]

        with open(fileName) as self.json_data:
            self.jsonMap = json.load(self.json_data)
        print("Json file loaded in ")

        self.nodes=[]
        for element in self.jsonMap["NOD"]:
            self.nodes.append(Node(element['NAME'].encode("utf-8"),element['OUT_BACK'].encode("utf-8"),element['OUT_RIGHT'].encode("utf-8"),element['OUT_AHEAD'].encode("utf-8"),element['OUT_LEFT'].encode("utf-8"),element['COORDINATES']))
    
            
    def getNodes(self):
        return self.nodes
	
	
    def getNodeByCoordinates(self, x,y):
		for node in self.nodes:
			if ((node.getCoord()[0] == x) and (node.getCoord()[1] == y)):
				return node.getName()
		return 0

#testMap = Map("Map.Json")	
