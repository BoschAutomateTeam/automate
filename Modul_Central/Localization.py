import PathCalculator2 as PC
import CarClient as CC
import ReadMap as RM
import math
import copy
import time
from threading import Thread


class Localization(Thread):
        def __init__(self, threadID, name):
                Thread.__init__(self)
                self.threadID = threadID
                self.name = name
                self.speed=0.2 # DE VAZUT IN CE UNITATE DE MASURA
                self.raza1=0
                self.raza2=0
                self.delta_t=0
                self.delta_r=0
                self.current_time=time.time()
                self.ORIGIN =[0,0]
                self.current_index = 0;
                self.next_index = 1;
                self.map = RM.Map('HARTA_OVALA_NOUA.json')
                self.pathCalculator = PC.PathCalculator(self.map.getNodes())
                self.path = self.pathCalculator.getOptimalPath(self.map.getNodeByCoordinates(0.9,0), self.map.getNodeByCoordinates(2.25,0.9))
                self.current_position = self.pathCalculator.getNodeByName(self.path[self.current_index][0]).getCoord()
                self.next_position = self.pathCalculator.getNodeByName(self.path[self.next_index][0]).getCoord()
                self.GPS_TIME = 1
                self.GPS_PREV_TIME = 0;
                self.gpsPosition = [CC.car_poz.real/10, CC.car_poz.imag/10]
                print(self.gpsPosition)

        def calculate_delta_r(self,point_1,point_2):
                self.raza1=math.sqrt(math.pow(self.ORIGIN[0]-point_1[0],2)+math.pow(self.ORIGIN[1]-point_1[1],2))
                self.raza2=math.sqrt(math.pow(self.ORIGIN[0]-point_2[0],2)+math.pow(self.ORIGIN[1]-point_2[1],2))
                return math.fabs(self.raza1 - self.raza2)

        def isTimeToChange(self,t):
                print('A')
                if (self.current_time + self.delta_t >= t):
                        print('Conditie schimbare indeplinita')
                        return True

        #DE LUCRAT AICI
        def isTimeForGPSChange(self,t):
                #self.gpsPosition = gps.getPosition()
                if (t - self.current_time - self.GPS_PREV_TIME >= self.GPS_TIME):
                        self.GPS_PREV_TIME = t
                        self.distanceToCurrentPoint = math.sqrt(math.pow(self.gpsPosition[0]-self.pathCalculator.getNodeByName(self.path[self.current_index][0]).getCoord()[0],2) +
                                                                math.pow(self.gpsPosition[1]-self.pathCalculator.getNodeByName(self.path[self.current_index][0]).getCoord()[1],2))

                        self.distanceToNextPoint = math.sqrt(math.pow(self.gpsPosition[0]-self.pathCalculator.getNodeByName(self.path[self.next_index][0]).getCoord()[0],2) +
                                                             math.pow(self.gpsPosition[1]-self.pathCalculator.getNodeByName(self.path[self.next_index][0]).getCoord()[1],2))

                        if(self.distanceToCurrentPoint < self.distanceToNextPoint):
                                pass
                        else:
                                print('GPS a intrat la timp curent = ' + str(self.current_time))
                                self.current_index += 1;
                                self.next_index +=1;
                                self.current_time = t
                                self.current_position = self.pathCalculator.getNodeByName(self.path[self.current_index][0]).getCoord()
                                self.next_position = self.pathCalculator.getNodeByName(self.path[self.next_index][0]).getCoord()
                        return True
                else:
                        return False

        def changePosition(self,t):
                print('Change position ')
                if(self.isTimeToChange(t)):
                        t = time.time()
                        self.current_time = t
                        print('S-a modificat pozitia la timp curent = ' + str(t) + 'la index = ' + str(self.current_index));
                        self.current_index+=1;
                        self.next_index+=1;
                        try:
                                self.current_position = self.pathCalculator.getNodeByName(self.path[self.current_index][0]).getCoord()
                                self.next_position = self.pathCalculator.getNodeByName(self.path[self.next_index][0]).getCoord()
                        except:
                                print('Nu mai calculex indicii.')

        def isEndOfTheRoad(self):
                if(self.next_index == len(self.path)):
                        return True
                else:
                        return False

        def run(self,output):
                while (self.isEndOfTheRoad() == False):
                        time.sleep(0.1)
                        self.delta_r = self.calculate_delta_r(self.current_position, self.next_position)
                        print('DeltaR = ' + str(self.delta_r))
                        self.delta_t = self.delta_r / self.speed
                        print('DeltaT =  ' + str(self.delta_t))
                        if(self.isEndOfTheRoad() == False):
                                if(self.isTimeForGPSChange(time.time()) == False):
                                        self.changePosition(time.time())
                                        output.put([self.current_position,self.next_position])
                                else:
                                        print('GPS')
                        else:
                                print('EXIT')

##TESTE##
'''
L = Localization('1', 'LocalizationThread')
print(L.calculate_delta_r([0,1],[0,2]))
time.sleep(10)
L.start()
'''
#L.run()
