from time import sleep
import time
import numpy as np
import cv2
import threading
import queue
import math
import copy
import Localization
import Camera_Vision

INITIAL = 0
PARKING = 1
RUNNING = 2
CURVE = 3
TRANSITION = 4
INTERSECTION = 5
'''
Vars used:
    Command center:
        -state -> defines the system's state
        -finished -> checks if the car has finished its course, used for the loop

Methods used:
    Command center:
        -printState -> prints state
        -turnOn -> checks the system if it's okay, processes the map
'''
global serialHandler
class CommandCenter:
    def __init__(self):
        self.state = INITIAL
        self.turn_on()
    def car_run(self):
        
        print("Init stacks:")
        cameraQueue =  queue.Queue()
        accelQueue =  queue.Queue()
        gpsQueue = queue.Queue()
        print("Init threads")
        #Obiect nou sau folosim functiile din clasa?
        #Vedem si testam
        cameraThread = threading.Thread(target = Camera_Vision.Camera.start(), args  = (cameraQueue,))
        gpsThread = threading.Thread(target = Localization.run(), args  = (gpsQueue,))
        print("Starting threads")
        cameraThread.start()
        gpsThread.start()
        print("Running main loop")
        loop = True
        while(True):
            time.sleep(0.1)
            #super basic, neoptimizat pt semn (inca)
            #pastram functionalitate pana implementam logica de semn
            #Oare chiar trebuie sa fac obiect din cele 2 clase???
            #Dubasa implementarea 
            if not gpsQueue.empty():
                #In gpsCoords avem [current_position, next_position]
                gpsCoords = cameraQueue.get()
                #Verificam parcarea
                #Verificam stop
                #Verificam intersectie
                #Proof of concept
                #Cautam parcarea
                #Cautam stopul
                #Cautam intersectia
                #In caz de niciunul, avem conditii de drum normale, deci putem rula normal camera
                if gpsCoords[1].sign == 'PARK':
                    #Nu stiu daca va merge functia _stop()
                    cameraThread._stop()
                    self.stop()
                    self.park()
                    #De aici ia dreapta si porneste iar
                    #De calibrat miscarile si de gasit o miscare de parcare decenta
                    #Pentru cazurile posibile 
                else if gpsCoords[1].sign = 'STOP':
                    self.stop()
                else if gpsCoords[1].sign = 'INTERSECT':
                    cameraThread._stop()
                    #Aici atacam cazurile in care luam stanga dreapta fata
                    #De intrebat Cristi de unde iau cazurile respective
                    #Faza cu masina la sf intersectie pornit camera
                    #Posibile probleme, camera preia imagini "din urma" ce vor sa fie procesate
                    #Rezultat: unghiuri pentru curbe sau altele, si nu pentru drumul ce urmeaza
                    #Posibila solutie, stinge threadul de camera pt chestii de genul odata ce e tot ok
                    #Inca una (paranoic mode), curatam coada de camera
                    #De gandit
                    
                else if not cameraQueue.empty() && loop == True:
                    angle = cameraQueue.get()
                    self.move(serialHandler,15-abs(angle),angle)
               ### if Localization.isEndOfTheRoad() 
            

                
    def turn_on(self):
        print("Initialising command center:")
    def move(self,serialHandler,speed,angle):
        e = SaveEncoder.SaveEncoder("EncoderPID%.2f"%speed+".csv")
        e.open()
        ev1=threading.Event()
        serialHandler.readThread.addWaiter("MCTL",ev1,e.save)
        sent=serialHandler.sendMove(speed,angle)
        if sent:
            ev1.wait()
            ev1.clear()
            serialHandler.readThread.deleteWaiter("MCTL",ev1)
            time.sleep(0.1)
    #def getSignDistance:
    def stop():
        self.move(serialHandler,0,0)
    def park():
        angle=23
        pwm=8
        secunde=1.3
        self.move(0,-angle)
        time.sleep(0.1)
        self.move(-pwm,angle)
        time.sleep(secunde)
        self.move(0.0)
        self.move(0,-angle)
        time.sleep(0.1)    
        self.move(-pwm,-angle)
        time.sleep(seunde)
        self.move(0.0)
        self.move(pwm,0)
        time.sleep(0.8)
        self.move(0.0)

    def printState(self):
        print(self.state)




x = CommandCenter()


