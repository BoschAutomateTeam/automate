from picamera import PiCamera
from time import sleep
from picamera.array import PiRGBArray
import time
import cv2


import SaveEncoder
import SerialHandler
serialHandler = SerialHandler.SerialHandler()
camera = PiCamera()
camera.resolution = (640,480)
camera.framerate = 32
rawCapture = PiRGBArray(camera, size=(640,480))

time.sleep(0.1)


pwm=0.0
#e=SaveEncoder.SaveEncoder("EncoderPID%.2f"%pwm+".csv")
#e.open()
    
#ev1=threading.Event()
#ev2=threading.Event()
#serialHandler.readThread.addWaiter("MCTL",ev1,e.save)
#serialHandler.readThread.addWaiter("BRAK",ev1,e.save)
#serialHandler.readThread.addWaiter("ENPB",ev2,e.save)
camera.color_effects = (128,128)
angle = 0
pwm = 0
for frame in camera.capture_continuous(rawCapture, format = 'bgr', use_video_port = True):
    image = frame.array
    cv2.imshow("Frame", "cars.jpg")

    key = cv2.waitKey(1) &0xFF
    
        
    if key == ord("w"):
        pwm = 7
        angle = 0

        
    if key == ord(" "):
        pwm = 0
        angle = 0
        
    if key == ord("a"):
        angle += -23.0
        
    if key == ord("d"):
        angle += 23.0
        
        
    if key == ord("s"):
        pwm = -7.0
    serialHandler.sendBrake(0.0)
        
    serialHandler.sendMove(pwm,angle) 
    rawCapture.truncate(0)
    if key == ord("q"):
        break



