
#!/usr/bin/python

import thread
import threading
import SerialHandler
from picamera import PiCamera
from time import sleep
from picamera.array import PiRGBArray
import time
import cv2
# Define a function for the thread

exitflag = 0
'''Clasa thread preclurare imagini/captura de la camera'''
class cameraControlThread (threading.Thread):
   def __init__(self, threadID, name, counter):
      threading.Thread.__init__(self)
      self.threadID = threadID
      self.name = name
      self.counter = counter
      self.camera = PiCamera()
      self.camera.resolution = (640,480)
      self.camera.framerate = 32
      self.camera.color_effects = (128,128)
      self.rawCapture = PiRGBArray(self.camera, size=(640,480))
      self.initializat = 0
      time.sleep(0.1)
      self.key = ""

   def run(self):
      print "Starting " + self.name + " "
      for frame in self.camera.capture_continuous(self.rawCapture, format = 'bgr', use_video_port = True):
          print("Running Camera")
          self.image = frame.array
          cv2.imshow("Frame", self.image)
          self.key = cv2.waitKey(1)
          self.initializat = 1
          self.rawCapture.truncate(0)
      print "Exiting " + self.name

   def getKey(self):
      print("apel")
      return self.key;

'''Clasa thread control deplasare'''
class movingControlThread(threading.Thread):
   def __init__(self, threadID, name, counter):
      threading.Thread.__init__(self)
      self.threadID = threadID
      self.name = name
      self.counter = counter
      #Viteza masinii
      self.pwm = 0
      #Ungiul de rotatie al masinii
      self.angle = 0
      #Managerul transmisiilor RSPY -> Nucleo
      self.serialHandler = SerialHandler.SerialHandler()

   def run(self):
      while True :
         print("Runing Moving")
         self.key = cv2.waitKey(1)
         if self.key == ord("w"):
            if(self.pwm<10):
               if (self.pwm == 0):
                  self.pwm = 7
               else:
                  self.pwm = self.pwm + 1
         if self.key == ord("s"):
            if(self.pwm>-10):
               if (self.pwm >= 0):
                  self.pwm = -7
               else:
                  self.pwm = self.pwm - 1
         if self.key == ord("d"):
            if self.angle < 22:
               self.angle += 2
         if self.key == ord("a"):
            if self.angle > -22:
               self.angle -= 2
         if self.key == ord("f"):
            self.pwm = 0
            self.angle = 0

         self.serialHandler.sendMove(self.pwm,self.angle)

# Create new threads
thread1 = cameraControlThread(1, "Thread-Camera", 4)
thread2 = movingControlThread(2, "Thread-Deplasare",2)
# Start new Threads
thread1.start()
thread2.start()
print("Threaduri pornite")
