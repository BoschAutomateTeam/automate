import cv2 as cv2
import numpy as np
import time
import math
from threading import Thread
from picamera.array import PiRGBArray
from picamera import PiCamera
import SerialHandler
import SaveEncoder
#import Localization


class Camera(Thread):
    ITERATION_STEP = 40
    WHEEL_STEPS = 23
    LEFT_REFERENCE = 30
    RIGHT_REFERENCE = 20
    EDGE_DILATATION = 17
    IMAGE_DILATATION = 7
    SIGN_SIZE = 100
    wheel_angle = 0
    stop_state = False
    park_state = False
    #----------OPTIONAL----------#
    pwm = 7
   
    # CONSTRUCTOR DEFIFINITON
    def __init__(self):
        self.clahe = cv2.createCLAHE(clipLimit=1.0, tileGridSize=(30,30))
        self.camera = PiCamera()
        self.camera.resolution = (640, 480)
        self.camera.framerate = 32
        self.rawCapture = PiRGBArray(self.camera, size=(640, 480))
        print("Camera was set up...")
        self.h = 480
        self.w = 640
        self.width = int(self.w/2)
        self.third_part = int((2*self.h)/3)
        self.wheel_angle = 0
        self.pwm = 7
        pwm = 7
        print("Variables was initialized...")
        self.serialHandler = SerialHandler.SerialHandler()
        print("Serial handler was created...")
        Thread.__init__(self)
        print("New thread was created...")
    # END constructor

    # COMPUTE POINTS METHODS START HERE
    #---------------------------------------------------------------------------------------------------

    # This method look for two point on the left and two points on the right
    # Each set of points must describe a straight-line which descripes the road left and right curves
    def get_points(self, image, width, im_width, im_height):
        ok_up_left = 0
        ok_up_right = 0
        ok_down_left = 0
        ok_down_right = 0
        up_l = [0, 0]
        down_l = [0, 0]
        up_r = [0, 0]
        down_r = [0, 0]
        # Iterare into square 320 x 320
        for i in range(0, width, self.ITERATION_STEP):
            for j in range(0, width, self.ITERATION_STEP):

                # Iterate for UP-LEFT point
                x = im_height - j - 1
                y = width - i
                if image[x, y] == 255 and ok_up_left == 0:
                    up_l = [y, x]
                    ok_up_left = 1

                # Iterate for UP-RIGHT point
                x = im_height - j - 1
                y = width + i - 1
                if image[x, y] == 255 and ok_up_right == 0:
                    up_r = [y, x]
                    ok_up_right = 1

                # Iterate for DOWN-RIGHT point
                x = im_height - j - 1
                y = width + i - 1
                if image[x, y] == 255 and ok_down_right == 0:
                    down_r = [y, x]
                    ok_dowm_right = 1

                # Iterate for for DOWN-LEFT point
                x = im_height - i - 1
                y = width - j
                if image[x, y] == 255 and ok_down_left == 0:
                    down_l = [y, x]
                    ok_down_left = 1

                # If point are found loop out
                if ok_up_left == 1 and ok_up_right == 1 and ok_down_right == 1 and ok_down_left == 1:
                    break
            # If point are found loop out
            if ok_up_left == 1 and ok_up_right == 1 and ok_down_right == 1 and ok_down_left == 1:
                break
        # RETURN the computed points
        
        return down_l, up_l, down_r, up_r
    # END of get_points method

    # Calculate angles of the RIGHT with Y-axis from DOWN-RIGHT point
    # Calculate angles of the LEFT with Y-axis from DOWN-LEFT point
    def get_angles(self, dl, ul, dr, ur):
        down_l = ul[0] - dl[0]
        up_l = ul[1] - dl[1]
        down_r = ur[0] - dr[0]
        up_r = ur[1] - dr[1]
        # LEFT-Line slope computing
        if down_l == 0:
            slope_l = 0
        else:
            slope_l = float(up_l)/down_l

        # RIGHT-Line slope computing
        if down_r == 0:
            slope_r = 0
        else:
            slope_r = float(up_r)/down_r

        # Computing angle between LEFT-LINE and Y-axis from DOWN-LEFT point
        left_angle = 90 + int(math.atan(slope_l)*57.2557795)

        # Computing angle between RIGHT-LINE and Y-axis from DOWN-RIGHT point
        right_angle = 90 - int(math.atan(slope_r)*57.2557795)

        # Return the computed angles
        return left_angle, right_angle
    # END of get_angles method

    # Convert Angle to Wheel Angle
    def apply_road_rule_for_left(self, angle):
        if angle < self.LEFT_REFERENCE:
            return -int(float(self.LEFT_REFERENCE - angle)/self.LEFT_REFERENCE*self.WHEEL_STEPS)
        angle = angle - self.LEFT_REFERENCE
        return int(float(angle)/(90 - self.LEFT_REFERENCE)*self.WHEEL_STEPS)
    # END of apply_road_rule_for_left method

    # Convert Angle to Wheel Angle
    def apply_road_rule_for_right(self, angle):
        if angle < self.RIGHT_REFERENCE:
            return int(float(self.RIGHT_REFERENCE-angle)/self.RIGHT_REFERENCE*self.WHEEL_STEPS)
        angle = angle - self.RIGHT_REFERENCE
        return -int(float(angle)/(90-self.RIGHT_REFERENCE)*self.WHEEL_STEPS)
    # END of apply_road_rule_for_right method

    # Compute relative inclination
    def relative_inclination(self, angle, reference):
        return angle - reference
    # END of relative_inclination method
    # COMPUTE POINTS METHODS END HERE------------------------------------------------------------------

    # IMAGE CONVERSION METHOD STARTS HERE
    #---------------------------------------------------------------------------------------------------
    # Computer a binary polygon image from given vertices
    def filter_region(self, image, vertices):
        mask = np.zeros_like(image)
        if len(mask.shape) == 2:
            cv2.fillPoly(mask, vertices, 255)
        else:
            cv2.fillPoly(mask, vertices, (255,)*mask.shape[2])
        return mask
    # END of filter_region method

    # Restrict view to a polygon
    def select_region(self, image):
        rows, cols = image.shape[:2]
        bottom_left = [cols*0.0, rows*1]
        top_left = [cols*0.3, rows*0.6]
        bottom_right = [cols*1, rows*1]
        top_right = [cols*0.7, rows*0.6]
        # the vertices are an array of polygons and the data type must be integer
        vertices = np.array(
            [[bottom_left, top_left, top_right, bottom_right]], dtype=np.int32)
        return self.filter_region(image, vertices)
    # END of select_region method

    # Brightness setup
    def adjust_gamma(self, image, gamma=0.4):
        invGamma = 1.0 / gamma
        table = np.array([((i / 255.0) ** invGamma) * 255
                          for i in np.arange(0, 256)]).astype("uint8")
        # apply gamma correction using the lookup table (LUT)
        return cv2.LUT(image, table)
    # END of adjust_gamma method

    # Converts the image in order to be processed
    def convert_img(self, image):
        # 2 x 2 one matrix
        one = np.ones((2, 2), np.uint8)
        # dilate pixels to get a better range of colors
        #dilate_image = cv2.dilate(image,one, iterations= self.IMAGE_DILATATION)
        # convert image to gray
        #image_to_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        #apply histogram to image
        c_histo = self.clahe.apply(image)
        # bright or un bright the image
        bright_image = self.adjust_gamma(c_histo)
        smooth = cv2.medianBlur
        # detect edges from image
        edges = cv2.Canny(c_histo, 50, 250)
        # turn upside part of the image into black color
        edges[0:self.third_part, 0:self.w] = 0
        # dilate white pixel to get a better recognition
        dilate = cv2.dilate(edges, one, iterations= self.EDGE_DILATATION)
        # create a polygonal mask
        mask = self.select_region(dilate)
        # get comon parts from mask and our frame
        region = cv2.bitwise_and(dilate, mask)
        

        return region
    # END of convert_img method
    # IMAGE CONVERSION METHODs END HERE----------------------------------------------------------------

    # INFO METHODS START HERE
    #-------------------------------------------------------------------------------------------------
    # Print the parameters in console
    def print_info(self, wheel_angle, angle, al, ar, down_l, up_l, down_r, up_r, time):
        print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
        print("########################################################")
        print("# Dimension: ", str(self.h), " x ", str(self.w), "px")
        print("# Points: ", down_l, up_l, down_r, up_r)
        print("# Angle left: ", al)
        print("# Angle right: ", ar)
        print("# Follow Angle: ", angle)
        print("# WHEEL Angle: ", wheel_angle)
        print("# Time :", time)
        print("########################################################\n")
    # END of print_info method

    # Show images
    def show_frames(self, image, converted_image):
        cv2.imshow('Video', image)
        cv2.imshow('Converted Video', converted_image)
    # END of show_frames method

    # Draw lines
    def draw_lines(self, frame, dl, ul, dr, ur):
        cv2.line(frame, (dl[0], dl[1]), (ul[0], ul[1]), [0, 0, 255], 5)
        cv2.line(frame, (dr[0], dr[1]), (ur[0], ur[1]), [0, 0, 255], 5)
        cv2.line(frame, (self.width, 0), (self.width, self.h), [0, 255, 0], 3)
        return frame
    # END of draw_lines method
    # INFO METHODS END HERE-----------------------------------------------------------------------------
    def sign_detection(self, park_sign_array, stop_sign_array):
        if stop_sign_array.__len__() > 0:
            if stop_sign_array[0][3] > self.SIGN_SIZE:
                self.stop_state = True
                print("Sign size:", stop_sign_array[0][3])
                print("STOP SIGN DETECTED...\nWAIT 5 SECONDS AND GO...")
                self.serialHandler.sendMove(self.pwm, 0)
            else:
                self.stop_state = False
        else:
            self.stop_state = False
        if park_sign_array.__len__() > 0:
            if park_sign_array[0][3] > self.SIGN_SIZE:
                self.park_state = True
                print("Sign size:", park_sign_array[0][3])
                print("PARK SIGN DETECTED...\nWAIT 5 SECONDS...\nINITIATE PARK MODE...")
                self.serialHandler.sendMove(self.pwm, 0)
            else:
                self.park_state = False
        else:
            self.park_state = False
    # Getter method
    def get_angle(self):
        return self.wheel_angle
    # END of getter method

    # RUN THE THREAD
    def run(self):
        print("Run method was called...")
        angle = 0
        for image in self.camera.capture_continuous(self.rawCapture, format='bgr', use_video_port=True):
            frame = image.array
            #print("Frame read-PASSED...")
            # frame start time
            start = time.time()
            #print("Init time-PASSED...")
            # truncate image in order to be processed
            self.rawCapture.truncate(0)
            #print("Truncate-PASSED...")

            # convert frame
            image_to_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            convert_result = self.convert_img(image_to_gray)
            #print("Conver frame-PASSED...")

            # compute points
            dl, ul, dr, ur = self.get_points(convert_result, self.width, self.w, self.h)
            #print("Points detection-PASSED...")

            # get angles
            al, ar = self.get_angles(dl, ul, dr, ur)
            #print("Angle computing-PASSED...")
            if al >= 90:
                al = -1
            if ar >= 90:
                ar = 1

            # Angle selection
            relative_left = self.relative_inclination(al, self.LEFT_REFERENCE)
            relative_right = self.relative_inclination(ar, self.RIGHT_REFERENCE)

            if (relative_left < relative_right):
                angle = al
                self.wheel_angle = self.apply_road_rule_for_left(al)
                if ((al < 1 or al > 90) and  (ar > 1 and ar <90)):
                    angle = ar
                    self.wheel_angle = self.apply_road_rule_for_right(ar)
                if ((ar < 1 or al > 90) and  (al > 1 and al <90)):
                    angle = al
                    self.wheel_angle = self.apply_road_rule_for_left(al)
                
            else:
                angle = ar
                self.wheel_angle = self.apply_road_rule_for_right(ar)
                if ((al < 1 or al > 90) and  (ar > 1 and ar <90)):
                    angle = ar
                    self.wheel_angle = self.apply_road_rule_for_right(ar)
                if ((ar < 1 or al > 90) and  (al > 1 and al <90)):
                    angle = al
                    self.wheel_angle = self.apply_road_rule_for_left(al)
            
            
            self.serialHandler.sendMove(self.pwm, self.get_angle())
            #------------------OPTIONAL------------------#
            draw = self.draw_lines(frame, dl, ul, dr, ur)
            self.show_frames(draw, convert_result)
            self.print_info(self.get_angle(), angle, al, ar, dl, ul, dr, ur, time.time() - start)
            #print("Show infos-PASSED...")
            k = cv2.waitKey(1) & 0xff
            if k == 27:
                break
        print("Unexpected exit from camera loop...")
   # END of run method


cam = Camera()
print("Camera object was created")
cam.start()
print("Camera thread started...")
time.sleep(6)

print("#############################################")
print("###########    NEW TEST      ################")
print("#############################################")
#serialHandler.startReadThread()

L = Localization('1', 'LocalizationThread')
L.start()
time.sleep(5)
#angle = 0
#serialHandler.sendMove(pwm,0)
#time.sleep(3)
#serialHandler.sendMove(0,0)
