import time
import numpy as np
import cv2 as cv2
#from parking import parking
import threading
import SerialHandler
import SaveEncoder
from random import randint
import queue
import math
import copy
import ReadMap as RM
import PathCalculator2 as PathCalc
#from picamera.array import PiRGBArray
import RoadDetectorPC as RoadDetect
INITIAL = 0
PARKING = 1
RUNNING = 2
CURVE = 3
TRANSITION = 4
INTERSECTION = 5
'''
Vars used:
    Command center:
        -state -> defines the system's state
        -finished -> checks if the car has finished its course, used for the loop

Methods used:
    Command center:
        -printState -> prints state
        -turnOn -> checks the system if it's okay, processes the map
'''
global serialHandler
global thread
class CommandCenter:
    def __init__(self):
        '''
        Initialisation
        Calculate the shortest path on the map
        Configure the car
        '''        
        self.state = INITIAL
        self.map = RM.Map('HARTA_OVALA_NOUA.json')
        self.pathCalculator =    PathCalc.PathCalculator(self.map.getNodes())
        self.optimalPath = self.pathCalculator.getOptimalPath(self.map.getNodeByCoordinates(0.9,0), self.map.getNodeByCoordinates(2.25,0.9))
        self.cameraQueue =  queue.Queue()
        self.accelQueue =  queue.Queue()
        self.gpsQueue = queue.Queue()
        self.camera =  RoadDetect.Camera("park_video.avi", 'stop_file.xml', 'park_file.xml')
        
        self.car_run()
    def car_run(self):
        thread = threading.Thread(target = self.camera.run, args = (self.cameraQueue,))
        thread.start()
        while(True):
           # serialHandler = SerialHandler.SerialHandler()
           # serialHandler.startReadThread()
            lastParkSignDistance = 9999
            cameraSign = None
           # self.move(serialHandler,0) 
             #cameraThread = threading.Thread(target = cam.getAngle, args(cameraQueue,))
             #cameraThread.start()
            time.sleep(0.1)
            #super basic, neoptimizat pt semn (inca)
            #pastram functionalitate pana implementam logica de semn
            #TODO
            while not self.cameraQueue.empty():
                #angle = cameraQueue.get()
                #move(15-abs(angle),angle)
                print(self.cameraQueue.get())
    def turn_on(self):
        print("Initialising command center:")
        map = RM.Map('HARTA_OVALA_NOUA.json')
        pathCalculator = PathCalc.PathCalculator(map.getNodes())

        #p va fi drumul minim. Apelul metodei presupune apelul cu nummele nodurilor. map.getNodeByCoordinates(x,y) va returna numele nodului cu coordonatele x,y
        P = pathCalculator.getOptimalPath(map.getNodeByCoordinates(0.9,0), map.getNodeByCoordinates(2.25,0.9))
        
        #camera = cam.Camera()
        self.car_run()  
    def move(self,serialHandler,speed,angle):
        e = SaveEncoder.SaveEncoder("EncoderPID%.2f"%speed+".csv")
        e.open()
        ev1=threading.Event()
        serialHandler.readThread.addWaiter("MCTL",ev1,e.save)
        sent=serialHandler.sendMove(speed,angle)
        if sent:
            ev1.wait()
            ev1.clear()
            serialHandler.readThread.deleteWaiter("MCTL",ev1)
            time.sleep(0.1)
    #def getSignDistance:
    #def stop():
    #def park():


    def printState(self):
        print(self.state)




x = CommandCenter()


