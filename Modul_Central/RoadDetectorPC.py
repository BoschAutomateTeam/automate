import cv2
import numpy as np
import time
import math

import os


class Camera():
    ITERATION_STEP = 40
    WHEEL_STEPS = 23
    LEFT_REFERENCE = 30
    RIGHT_REFERENCE = 20
    EDGE_DILATATION = 17
    IMAGE_DILATATION = 7
    SIGN_SIZE = 100
    wheel_angle = 0
    stop_state = False
    park_state = False
  
    # CONSTRUCTOR DEFIFINITON

    def __init__(self, source, stop_xml, park_xml):
       # print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
        self.clahe = cv2.createCLAHE(clipLimit=1.0, tileGridSize=(30, 30))
        self.capture = cv2.VideoCapture(source)
       # print("Camera was set up...")
        self.h = int(self.capture.get(cv2.CAP_PROP_FRAME_HEIGHT))
        self.w = int(self.capture.get(cv2.CAP_PROP_FRAME_WIDTH))
        self.width = int(self.w/2)
        self.third_part = int((2*self.h)/3)
       # print("Variables were initialized...")
        self.stop_cascade = cv2.CascadeClassifier(stop_xml)
        self.park_cascade = cv2.CascadeClassifier(park_xml)
       # print("Haar cascade files were read...")
       # Thread.__init__(self)
        #print("Wait for 3 seconds...")
       # time.sleep(3)

    # END constructor
    # COMPUTE POINTS METHODS START HERE
    #---------------------------------------------------------------------------------------------------

    # This method look for two point on the left and two points on the right
    # Each set of points must describe a straight-line which descripes the road left and right curves
    def get_points(self, image, width, im_width, im_height):
        ok_up_left = 0
        ok_up_right = 0
        ok_down_left = 0
        ok_down_right = 0
        up_l = [0, 0]
        down_l = [0, 0]
        up_r = [0, 0]
        down_r = [0, 0]
        # Iterare into square 320 x 320
        for i in range(0, width, self.ITERATION_STEP):
            for j in range(0, width, self.ITERATION_STEP):

                # Iterate for UP-LEFT point
                x = im_height - j - 1
                y = width - i
                if image[x, y] == 255 and ok_up_left == 0:
                    up_l = [y, x]
                    ok_up_left = 1

                # Iterate for UP-RIGHT point
                x = im_height - j - 1
                y = width + i - 1
                if image[x, y] == 255 and ok_up_right == 0:
                    up_r = [y, x]
                    ok_up_right = 1

                # Iterate for DOWN-RIGHT point
                x = im_height - j - 1
                y = width + i - 1
                if image[x, y] == 255 and ok_down_right == 0:
                    down_r = [y, x]
                    ok_dowm_right = 1

                # Iterate for for DOWN-LEFT point
                x = im_height - i - 1
                y = width - j
                if image[x, y] == 255 and ok_down_left == 0:
                    down_l = [y, x]
                    ok_down_left = 1

                # If point are found loop out
                if ok_up_left == 1 and ok_up_right == 1 and ok_down_right == 1 and ok_down_left == 1:
                    break
            # If point are found loop out
            if ok_up_left == 1 and ok_up_right == 1 and ok_down_right == 1 and ok_down_left == 1:
                break
        # RETURN the computed points

        return down_l, up_l, down_r, up_r
        # END of get_points method

    # Calculate angles of the RIGHT with Y-axis from DOWN-RIGHT point
    # Calculate angles of the LEFT with Y-axis from DOWN-LEFT point
    def get_angles(self, dl, ul, dr, ur):
        down_l = ul[0] - dl[0]
        up_l = ul[1] - dl[1]
        down_r = ur[0] - dr[0]
        up_r = ur[1] - dr[1]
        # LEFT-Line slope computing
        if down_l == 0:
            slope_l = 999999999
        else:
            slope_l = float(up_l)/down_l

        # RIGHT-Line slope computing
        if down_r == 0:
            slope_r = 999999999
        else:
            slope_r = float(up_r)/down_r

        # Computing angle between LEFT-LINE and Y-axis from DOWN-LEFT point
        left_angle = 90 + (math.atan(slope_l)*57.2557795)

        # Computing angle between RIGHT-LINE and Y-axis from DOWN-RIGHT point
        right_angle = 90 - (math.atan(slope_r)*57.2557795)

        # Return the computed angles
        return left_angle, right_angle
    # END of get_angles method

    # Convert Angle to Wheel Angle
    def apply_road_rule_for_left(self, angle):
        if angle < self.LEFT_REFERENCE:
            return -int(float(self.LEFT_REFERENCE - angle)/self.LEFT_REFERENCE*self.WHEEL_STEPS)
        angle = angle - self.LEFT_REFERENCE
        return int(float(angle)/(90 - self.LEFT_REFERENCE)*self.WHEEL_STEPS)
    # END of apply_road_rule_for_left method

    # Convert Angle to Wheel Angle
    def apply_road_rule_for_right(self, angle):
        if angle < self.RIGHT_REFERENCE:
            return int(float(self.RIGHT_REFERENCE-angle)/self.RIGHT_REFERENCE*self.WHEEL_STEPS)
        angle = angle - self.RIGHT_REFERENCE
        return -int(float(angle)/(90-self.RIGHT_REFERENCE)*self.WHEEL_STEPS)
    # END of apply_road_rule_for_right method

    # Compute relative inclination
    def relative_inclination(self, angle, reference):
        return angle - reference
    # END of relative_inclination method
    # COMPUTE POINTS METHODS END HERE------------------------------------------------------------------

    # IMAGE CONVERSION METHOD STARTS HERE
    #---------------------------------------------------------------------------------------------------
    # Computer a binary polygon image from given vertices
    def filter_region(self, image, vertices):
        mask = np.zeros_like(image)
        if len(mask.shape) == 2:
            cv2.fillPoly(mask, vertices, 255)
        else:
            cv2.fillPoly(mask, vertices, (255,)*mask.shape[2])
        return mask
    # END of filter_region method

    # Restrict view to a polygon
    def select_region(self, image):
        rows, cols = image.shape[:2]
        bottom_left = [cols*0.0, rows*1]
        top_left = [cols*0.3, rows*0.6]
        bottom_right = [cols*1, rows*1]
        top_right = [cols*0.7, rows*0.6]
        # the vertices are an array of polygons and the data type must be integer
        vertices = np.array(
            [[bottom_left, top_left, top_right, bottom_right]], dtype=np.int32)
        return self.filter_region(image, vertices)
    # END of select_region method

    # Brightness setup
    def adjust_gamma(self, image, gamma=0.4):
        invGamma = 1.0 / gamma
        table = np.array([((i / 255.0) ** invGamma) * 255
                          for i in np.arange(0, 256)]).astype("uint8")
        # apply gamma correction using the lookup table (LUT)
        return cv2.LUT(image, table)
    # END of adjust_gamma method

    # Converts the image in order to be processed
    def convert_img(self, image):
        cHisto = self.clahe.apply(image)
        # 2 x 2 one matrix
        one = np.ones((2, 2), np.uint8)
        # dilate pixels to get a better range of colors
        dilate_image = cv2.dilate(
            cHisto, one, iterations=self.IMAGE_DILATATION)
        #cv2.imshow("DILATE1", dilate_image)
        # bright or un bright the image
        bright_image = self.adjust_gamma(dilate_image)
        #cv2.imshow("BRIGHTED", bright_image)
        # detect edges from image
        edges = cv2.Canny(bright_image, 100, 250)
        #cv2.imshow("EDGES", edges)
        # turn upside part of the image into black color
        edges[0:self.third_part, 0:self.w] = 0
        # dilate white pixel to get a better recognition
        dilate = cv2.dilate(edges, one, iterations=self.EDGE_DILATATION)
        # create a polygonal mask
        mask = self.select_region(dilate)
        # get comon parts from mask and our frame
        region = cv2.bitwise_and(dilate, mask)

        return region
    # END of convert_img method
    # IMAGE CONVERSION METHODs END HERE----------------------------------------------------------------

    # INFO METHODS START HERE
    #-------------------------------------------------------------------------------------------------
    # Print the parameters in console
    def print_info(self, wheel_angle = 0, angle = 0, al = 0, ar= 0 , down_l = 0, up_l = 0, down_r = 0, up_r = 0, park_sign_array = [], stop_sign_array = [], time = 0):
        print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
        print("########################################################")
        print("# Dimension: ", str(self.h), " x ", str(self.w), "px")
        print("# Points: ", down_l, up_l, down_r, up_r)
        print("# Angle left: ", al)
        print("# Angle right: ", ar)
        print("# Follow Angle: ", angle)
        print("# WHEEL Angle: ", wheel_angle)
        print("# Stop sign: ", stop_sign_array)
        print("# Park sign: ", park_sign_array)
        print("# Time :", time)
        print("########################################################\n")
    # END of print_info method

    # Show images
    def show_frames(self, image, converted_image):
        cv2.namedWindow("Video")
        cv2.moveWindow("Video", 240, 10)
        cv2.imshow('Video', image)
        cv2.namedWindow("Converted Video")
        cv2.moveWindow("Converted Video", 880, 10)
        cv2.imshow('Converted Video', converted_image)
    # END of show_frames method

    # Draw lines
    def draw_lines(self, frame, dl, ul, dr, ur, stop_sign, park_sign):
        for (x, y, ww, hh) in park_sign:
            cv2.rectangle(frame, (x, y), (x+ww, y + hh), (190, 0, 0), 2)
        for (x, y, ww, hh) in stop_sign:
            cv2.rectangle(frame, (x, y), (x+ww, y + hh), (0, 0, 255), 2)
        cv2.line(frame, (dl[0], dl[1]), (ul[0], ul[1]), [0, 0, 255], 5)
        cv2.line(frame, (dr[0], dr[1]), (ur[0], ur[1]), [0, 0, 255], 5)
        cv2.line(frame, (self.width, 0), (self.width, self.h), [0, 255, 0], 3)
        cv2.line(frame, (0, int(self.h/2)),
                 (self.w, int(self.h/2)), [0, 255, 0], 3)

        return frame
    # END of draw_lines method
    # INFO METHODS END HERE-----------------------------------------------------------------------------

    # Getter method
    def get_angle(self):
        return self.wheel_angle
    # END of getter method

    # Getter method
    def get_stop_sign(self):
        return self.stop_state
    # END of getter method

    # Getter method
    def get_park_sign(self):
        return self.park_state
    # END of getter method

    # SHOW SIGN method
    def sign_detection(self, park_sign_array, stop_sign_array):
        if stop_sign_array.__len__() > 0:
            if stop_sign_array[0][3] > self.SIGN_SIZE:
                self.stop_state = True
                print("Sign size:", stop_sign_array[0][3])
                print("STOP SIGN DETECTED...\nWAIT 5 SECONDS AND GO...")
                time.sleep(5)
            else:
                self.stop_state = False
        else:
            self.stop_state = False
        if park_sign_array.__len__() > 0:
            if park_sign_array[0][3] > self.SIGN_SIZE:
                self.park_state = True
                print("Sign size:", park_sign_array[0][3])
                print("PARK SIGN DETECTED...\nWAIT 5 SECONDS...\nINITIATE PARK MODE...")
                time.sleep(5)
            else:
                self.park_state = False
        else:
            self.park_state = False
    # END of show_sign method

    # RUN THE THREAD
    def run(self, cameraQueue):
        
        angle = 0
        print(self.capture.isOpened())
        while True and self.capture.isOpened():
            # read a current frame
            _, frame = self.capture.read()

            st = time.time()
            # convert frame
            image_to_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            convert_result = self.convert_img(image_to_gray)

            # sign detection
            park_sign = self.park_cascade.detectMultiScale(
                image_to_gray, 1.3, 5)
            park_sign_array = np.array(park_sign)
            stop_sign = self.stop_cascade.detectMultiScale(
                image_to_gray, 1.3, 5)
            stop_sign_array = np.array(stop_sign)
            # compute points
            dl, ul, dr, ur = self.get_points(
                convert_result, self.width, self.w, self.h)

            # get angles
            al, ar = self.get_angles(dl, ul, dr, ur)

            if (al > 90 and al < 179):
                al = 180 - al
                ar = 180
            if (ar > 90 and ar < 179):
                ar = 180 - ar
                al = 180

            # Angle selection
            relative_left = self.relative_inclination(al, self.LEFT_REFERENCE)
            relative_right = self.relative_inclination(
                ar, self.RIGHT_REFERENCE)

            if (relative_left < relative_right):
                angle = al
                if ((al < 1 or al > 90) and (ar > 1 and ar < 90)):
                    angle = ar
                if ((ar < 1 or al > 90) and (al > 1 and al < 90)):
                    angle = al
                angle = self.apply_road_rule_for_left(angle)
            else:
                angle = ar
                if ((al < 1 or al > 90) and (ar > 1 and ar < 90)):
                    angle = ar
                if ((ar < 1 or al > 90) and (al > 1 and al < 90)):
                    angle = al
                angle = self.apply_road_rule_for_right(angle)
            if (ar < 1 or ar > 179) and (al < 1 or al > 179):
                angle = 0
            self.wheel_angle = angle
            '''
            #------------------OPTIONAL------------------#
            draw = self.draw_lines(frame, dl, ul, dr, ur, stop_sign, park_sign)
            self.print_info(self.get_angle(), angle, al, ar, dl, ul, dr, ur, park_sign_array, stop_sign_array, time.time() - st)
            self.show_frames(draw, convert_result)
            self.sign_detection(park_sign_array, stop_sign_array)
            #---------------END-OPTIONAL-----------------#
            '''
            cameraQueue.put(angle)
            k = cv2.waitKey(10) & 0xff
            if k == 27:
                break
        print("Exit from camera loop...")
   # END of run method


# os.system('cls')
# cam = Camera("park_video.avi", 'stop_file.xml', 'park_file.xml')
# print("Camera object was created")
# cam.start()
# print("Camera thread started...")
# while cam.is_alive():
   #  print(cam.get_angle(), cam.get_park_sign(), cam.get_stop_sign())

